<?php

/**
 * @file
 * Contains \Drupal\contact\Tests\Unit\MailHandlerTest.
 */

namespace Drupal\Tests\content_overviews\Unit;

use Drupal\content_overviews\Events\ContentOverviewsEvent;
use Drupal\content_overviews\Events\ContentOverviewsEventHandler;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @coversDefaultClass \Drupal\content_overviews\Events\ContentOverviewsEventHandler
 * @group content_overviews
 */
class ContentOverviewsEventHandlerTest extends UnitTestCase {

  private $enabled_data = array('article' => TRUE);
  private $mixed_data = array('article' => TRUE, 'page' => FALSE, 'other' => NULL);
  private $reversed_mixed_data = array('article' => FALSE, 'page' => TRUE, 'other' => NULL);
  private $empty_data = array();

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $loggerMock;

  /**
   * An interface to dispatch our events
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $eventDispatcherMock;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $configFactoryMock;

  /**
   * A Drupal::config mock object
   *
   * @var \stdClass|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $configMock;

  /**
   * Handler takes care of dispatching our events.
   *
   * @var \Drupal\content_overviews\Events\ContentOverviewsEventHandler
   */
  protected $eventHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->loggerMock = $this->getMock('\Psr\Log\LoggerInterface');
    $this->eventDispatcherMock = $this->getMock('\Symfony\Component\EventDispatcher\EventDispatcherInterface');
    $this->eventHandler = new ContentOverviewsEventHandler($this->eventDispatcherMock, $this->loggerMock);

    $this->setUpDrupalConfigMock();
  }

  /**
   * Helper function to make static calls to Drupal:config possible in a unit test
   * @see Drupal::config
   */
  private function setUpDrupalConfigMock() {
    // We need a mock for Drupal's core 'config.factory'
    $this->configFactoryMock = $this->getMock('\Drupal\Core\Config\ConfigFactoryInterface');
    // For the config object, we use a dummy mock object
    // because Drupal's config class construction adds unnecessary complexity for unit tests (has no interface)
    $this->configMock = $this->getMockBuilder('stdClass')
      //needed because otherwise stdClass as a mock has no methods at all
      ->setMethods(array('get', 'set', 'save'))
      ->getMock();

    // Make sure the factoryMock will always return the configMock for our settings
    $this->configFactoryMock->expects($this->any())
      ->method('get')
      ->with($this->equalTo('content_overviews.settings'))
      ->willReturn($this->configMock);

    // We now inject a real 'containerStub' into Drupal, set up with previously configured mocks
    $containerStub = new ContainerBuilder();
    $containerStub->set('config.factory', $this->configFactoryMock);
    \Drupal::setContainer($containerStub);
  }

  /**
   * Tests the handleEvent() method with an invalid event_name.
   *
   * @expectedException \Symfony\Component\Validator\Exception\InvalidArgumentException
   * @expectedExceptionMessage CO - Event name missing.
   *
   * @covers ::handleEvent
   */
  public function testHandleEventWithoutEventNameThrowsError() {
    $this->eventHandler->handleEvent("", $this->enabled_data);
  }

  /**
   * Tests the handleEvent() method with empty event_data.
   *
   * @covers ::handleEvent
   */
  public function testHandleEventWithEmptyDataDoesNothing() {
    $this->loggerMock->expects($this->once())
      ->method('warning')
      ->with($this->equalTo("No event dispatched for event_name. Data is missing."));

    $this->eventDispatcherMock->expects($this->never())
      ->method('handleEvent');

    $this->eventHandler->handleEvent("event_name", $this->empty_data);
  }

  /**
   * Tests the handleEvent() method with event_data.
   *
   * @covers ::handleEvent
   */
  public function testHandleEventWithDataDispatchesEvent() {
    $this->eventDispatcherMock->expects($this->once())
      ->method('dispatch')
      ->with($this->equalTo('content_overviews.event_name'),
        $this->equalTo(new ContentOverviewsEvent($this->enabled_data))
      );

    $this->loggerMock->expects($this->never())
      ->method('warning');
    $this->configMock->expects($this->never())
      ->method('save');

    $this->eventHandler->handleEvent("event_name", $this->enabled_data);
  }

  /**
   * Tests the handleContentTypeConfigAndEvent() method without config name.
   *
   * @expectedException \Symfony\Component\Validator\Exception\InvalidArgumentException
   * @expectedExceptionMessage CO - config name missing.
   *
   * @covers ::handleContentTypeConfigAndEvent
   */
  public function testHandleContentTypeConfigAndEventWithoutEventNameThrowsError() {
    $this->eventHandler->handleContentTypeConfigAndEvent("", $this->enabled_data, "event_name");
  }

  /**
   * Tests the handleContentTypeConfigAndEvent() method with empty config_data.
   *
   * @covers ::handleContentTypeConfigAndEvent
   */
  public function testHandleContentTypeConfigAndEventWithEmptyDataDoesNothing() {
    $config_name = "config_name";
    $this->loggerMock->expects($this->once())
      ->method('warning')
      ->with($this->equalTo("No config saved for $config_name. Data is missing."));

    $this->eventDispatcherMock->expects($this->never())
      ->method('handleEvent');

    $this->configMock->expects($this->never())
      ->method('get');
    $this->configMock->expects($this->never())
      ->method('save');

    $this->eventHandler->handleContentTypeConfigAndEvent($config_name, $this->empty_data, "event_name");
  }

  /**
   * Tests the handleContentTypeConfigAndEvent() method with config_data but without initial stored data.
   *
   * @covers ::handleContentTypeConfigAndEvent
   */
  public function testHandleContentTypeConfigAndEventWithNewData() {
    // expect a call to fetch this config from our configMock set up earlier
    $this->configMock->expects($this->once())
      ->method('get')
      ->with($this->equalTo('managed_content_types'))
      ->willReturn(array());

    $this->processAndAssertSavingManagedContentTypes($this->mixed_data);
  }

  /**
   * Tests the handleContentTypeConfigAndEvent() method with config_data and initial stored data.
   *
   * @covers ::handleContentTypeConfigAndEvent
   */
  public function testHandleContentTypeConfigAndEventWithExistingData() {
    // expect a call to fetch this config from our configMock set up earlier - original data
    $this->configMock->expects($this->once())
      ->method('get')
      ->with($this->equalTo('managed_content_types'))
      ->willReturn($this->reversed_mixed_data);

    $this->processAndAssertSavingManagedContentTypes($this->mixed_data);
  }

  /**
   * Helper function to run and assert ::handleContentTypeConfigAndEvent
   *
   * @param $input
   */
  private function processAndAssertSavingManagedContentTypes($input) {
    // expect a call to set the config in our configMock set up earlier - new data
    $this->configMock->expects($this->once())
      ->method('set')
      ->with($this->equalTo('config_name'),
        $this->equalTo(array('article' => TRUE))
      );

    // expect a call to save the config
    $this->configMock->expects($this->once())
      ->method('save');

    // expect the event being dispatched with new data
    $this->eventDispatcherMock->expects($this->once())
      ->method('dispatch')
      ->with($this->equalTo('content_overviews.event_name'),
        $this->equalTo(new ContentOverviewsEvent($input))
      );

    // No logging should occur
    $this->loggerMock->expects($this->never())
      ->method('warning');

    $this->eventHandler->handleContentTypeConfigAndEvent("config_name", $input, "event_name");
  }

}

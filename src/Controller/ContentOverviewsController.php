<?php

/**
 * @file
 * Contains \Drupal\content_overviews\Controller\ContentOverviewsController.
 */

namespace Drupal\content_overviews\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for content overviews.
 */
class ContentOverviewsController extends ControllerBase {

  /**
   * Displays the content-overviews administration settings page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function devel(Request $request) {
    return array(
      '#markup' => t('Hello World!'),
    );
  }

}

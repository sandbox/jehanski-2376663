<?php

namespace Drupal\content_overviews\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

class ContentOverviewsMenuLink extends MenuLinkDefault {

  public function isDeletable() {
    return TRUE;
  }

  public function deleteLink() {
  }

}
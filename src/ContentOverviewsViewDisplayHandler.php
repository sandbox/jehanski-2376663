<?php

namespace Drupal\content_overviews;


use Drupal\field\Entity\FieldConfig;
use Drupal\views\Views;

class ContentOverviewsViewDisplayHandler {

  /**
   * @param $entities
   * @return array
   */
  public function addDisplaysToView($entities) {
    // TODO: Check for fields that should be added.
    $types = node_type_get_names();
    $changes = array(
      'added' => array(),
      'updated' => array()
    );

    $view = Views::getView('content_overviews')->storage;
    foreach ($entities as $entity_name) {
      if (!isset($types[$entity_name])) {
        break;
      }
      $display = $view->get('display');
      if (isset($display['page_' . $entity_name])) {
        // TODO: Don't remove existing pane. Either only check for fields or do nothing.
        unset($display['page_' . $entity_name]);
        $view->set('display', $display);
        $changes['updated'][$entity_name] = $entity_name;
      }

      $entity_label = $types[$entity_name];
      $path = 'admin/co/' . $entity_name;
      $new_id = $view->addDisplay('page', $entity_label, 'page_' . $entity_name);
      $display = $view->get('display');
      $display[$new_id]['display_options']['field_langcode'] = '***LANGUAGE_language_content***';
      $display[$new_id]['display_options']['field_langcode_add_to_query'] = NULL;
      $display[$new_id]['display_options']['path'] = $path;
      $display[$new_id]['display_options']['display_description'] = '';
      $display[$new_id]['display_options']['title'] = $entity_label;

      $type = array(
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'relationship' => 'none',
        'group_type' => 'group',
        'admin_label' => '',
        'operator' => 'in',
        'group' => 1,
        'value' => array($entity_name => $entity_name),
        'exposed' => FALSE,
        'is_grouped' => FALSE,
        'plugin_id' => 'bundle',
      );

      $title = array(
        'id' => 'title',
        'table' => 'node_field_data',
        'field' => 'title',
        'operator' => 'contains',
        'exposed' => TRUE,
        'is_grouped' => FALSE,
        'group' => 1,
        'plugin_id' => 'string',
        'expose' => array(
          'operator_id' => 'title_op',
          'label' => 'Title',
          'operator' => 'title_op',
          'identifier' => 'title',
        ),
      );
      $status = array(
        'id' => 'status',
        'table' => 'node_field_data',
        'field' => 'status',
        'value' => FALSE,
        'exposed' => TRUE,
        'operator' => '=',
        'is_grouped' => FALSE,
        'group' => 1,
        'plugin_id' => 'boolean',
        'expose' => array(
          'label' => 'Published',
          'identifier' => 'status',
          'required' => FALSE,
        ),
      );
      $langcode = array(
        'id' => 'langcode',
        'table' => 'node_field_data',
        'field' => 'langcode',
        'value' => array(),
        'operator' => 'in',
        'exposed' => TRUE,
        'is_grouped' => FALSE,
        'group' => 1,
        'plugin_id' => 'boolean',
        'expose' => array(
          'operator_id' => 'langcode_op',
          'label' => 'Translation language',
          'operator' => 'langcode_op',
          'identifier' => 'langcode',
        ),
      );

      $display[$new_id]['display_options']['filters'] = array(
        'type' => $type,
        'title' => $title,
        'status' => $status,
        'langcode' => $langcode,
      );
      $display[$new_id]['display_options']['defaults'] = array(
        'filters' => FALSE,
        'filter_groups' => FALSE,
        'title' => FALSE,
      );
      $display[$new_id]['display_options']['filter_groups'] = array(
        'operator' => 'AND',
        'groups' => array(
          1 => 'AND',
        )
      );

      if (\Drupal::moduleHandler()
          ->moduleExists('content_translation') && content_translation_enabled('node', $entity_name)
      ) {
        $field_config = array(
          'id' => 'langcode',
          'table' => 'node_field_data',
          'field' => 'langcode',
          'relationship' => 'none',
          'group_type' => 'group',
          'admin_label' => '',
          'label' => 'Language',
          'exclude' => FALSE,
          'alter' => array(
            'alter_text' => FALSE,
            'text' => '',
            'make_link' => 'true',
            'path' => 'node/[nid]/translations',
            'absolute' => FALSE,
            'external' => FALSE,
            'replace_spaces' => FALSE,
            'path_case' => 'none',
            'trim_whitespace' => FALSE,
            'alt' => '',
            'rel' => '',
            'link_class' => '',
            'prefix' => '',
            'suffix' => '',
            'target' => '',
            'nl2br' => FALSE,
            'max_length' => '',
            'word_boundary' => TRUE,
            'ellipsis' => TRUE,
            'more_link' => FALSE,
            'more_link_text' => '',
            'more_link_path' => '',
            'strip_tags' => FALSE,
            'trim' => FALSE,
            'preserve_tags' => '',
            'html' => FALSE,
          ),
          'element_type' => '',
          'element_class' => '',
          'element_label_type' => '',
          'element_label_class' => '',
          'element_label_colon' => FALSE,
          'element_wrapper_type' => '',
          'element_wrapper_class' => '',
          'element_default_classes' => TRUE,
          'empty' => '',
          'hide_empty' => FALSE,
          'empty_zero' => FALSE,
          'hide_alter_empty' => TRUE,
          'link_to_node' => FALSE,
          'native_language' => 0,
          'plugin_id' => 'node_language',
        );
        $display[$new_id]['display_options']['fields'] = $display['default']['display_options']['fields'];
        $display[$new_id]['display_options']['defaults']['fields'] = FALSE;
        $display[$new_id]['display_options']['fields'] = $this->insertViewsFieldOnLastPositionWithOffset($display[$new_id]['display_options']['fields'], array('language' => $field_config), -2);
      }

      $view->set('display', $display);

      if (!isset($changes['updated'][$entity_name])) {
        $changes['added'][$entity_name] = 'view.content_overviews.page_' . $entity_name;
      }
    }

    if (count($changes['added']) || count($changes['updated'])) {
      $ret = $view->save();
      if ($ret == SAVED_UPDATED) {
        return $changes;
      }
    }
    return array();
  }

  /**
   * @param $entities
   * @return array
   */
  public function removeDisplaysFromView($entities) {
    $view = Views::getView('content_overviews')->storage;
    $removed_displays = array();
    foreach ($entities as $entity_name) {
      $display = $view->get('display');
      if (isset($display['page_' . $entity_name])) {
        unset($display['page_' . $entity_name]);
        $removed_displays[$entity_name] = $entity_name;
        $view->set('display', $display);
      }
    }
    if (count($removed_displays)) {
      $ret = $view->save();
      if ($ret == SAVED_UPDATED) {
        return $removed_displays;
      }
    }
    return array();
  }

  /**
   * @param $entity_name
   * @param $field_name
   * @return boolean
   */
  public function addFieldToDisplay($entity_name, $field_name) {
    $view = Views::getView('content_overviews')->storage;

    $field_needs_to_be_added = \Drupal::config('content_overviews.settings.managed_fields.' . $entity_name)
      ->get($field_name . '.add_as_column', 0);
    $is_field_sortable = \Drupal::config('content_overviews.settings.managed_fields.' . $entity_name)
      ->get($field_name . '.sort_column', 0);
    $is_field_exposed = \Drupal::config('content_overviews.settings.managed_fields.' . $entity_name)
      ->get($field_name . '.use_as_exposed_filter', 0);

    $display = $view->get('display');

    if (isset($display['page_' . $entity_name])) {
      $coFieldConfig = new ContentOverviewsFieldConfig($entity_name, $field_name);

      if ($field_needs_to_be_added) {
        $fields = $display['page_' . $entity_name]['display_options']['fields'];

        $this->setFieldsToDefaultIfNoFieldsToDisplay($entity_name, $fields, $display);


        $display['page_' . $entity_name]['display_options']['fields'] =
          $this->insertViewsFieldOnLastPositionWithOffset($fields, $coFieldConfig->generateContentOverviewsFieldConfig(), -2);

        if ($is_field_sortable) {
          $this->setSortsToDefaultIfNoSortsOnDisplay($entity_name, $display);

          $sort_info = $coFieldConfig->getViewsSortInfo();
          $display['page_' . $entity_name]['display_options']['sorts'][$sort_info['id']] = $sort_info;
        }
        else {
          if (isset($display['page_' . $entity_name]['display_options']['sorts']) && isset($display['page_' . $entity_name]['display_options']['sorts'][$field_name])) {
            unset($display['page_' . $entity_name]['display_options']['sorts'][$field_name]);
          }
        }
      }
      else {
        unset($display['page_' . $entity_name]['display_options']['fields'][$field_name]);
      }

      if ($is_field_exposed) {
        $this->setExposedFiltersToDefaultIfNoExposedFilters($entity_name, $display);

        $exposed_info = $coFieldConfig->getViewsExposedInfo();
        $display['page_' . $entity_name]['display_options']['filters'][$exposed_info['id']] = $exposed_info;
      }
      else {
        unset($display['page_' . $entity_name]['display_options']['filters'][$field_name]);
      }

      $view->set('display', $display);
      return $view->save() == SAVED_UPDATED;

    }

    return FALSE;
  }

  /**
   * Inserts a field on a given offset from the end of the fields list.
   *
   * @param $list
   * @param $field_to_add
   * @param $offset
   * @return mixed
   */
  private function insertViewsFieldOnLastPositionWithOffset($list, $field_to_add, $offset) {
    return array_merge(array_slice($list, 0, count($list) + $offset),
      $field_to_add,
      array_slice($list, $offset));
  }

  /**
   * @param $entity_name
   * @param $fields
   * @param $display
   */
  private function setFieldsToDefaultIfNoFieldsToDisplay($entity_name, $fields, &$display) {
    if (!isset($fields)) {
      $display['page_' . $entity_name]['display_options']['fields'] = $display['default']['display_options']['fields'];
      $display['page_' . $entity_name]['display_options']['defaults']['fields'] = FALSE;
    }
  }

  /**
   * @param $entity_name
   * @param $display
   */
  private function setSortsToDefaultIfNoSortsOnDisplay($entity_name, &$display) {
    if (!isset($display['page_' . $entity_name]['display_options']['sorts'])) {
      $display['page_' . $entity_name]['display_options']['sorts'] = $display['default']['display_options']['sorts'];
      $display['page_' . $entity_name]['display_options']['defaults']['sorts'] = FALSE;
    }
  }

  /**
   * @param $entity_name
   * @param $display
   */
  private function setExposedFiltersToDefaultIfNoExposedFilters($entity_name, &$display) {
    if (!isset($display['page_' . $entity_name]['display_options']['filters'])) {
      $display['page_' . $entity_name]['display_options']['filters'] = $display['default']['display_options']['filters'];
      $display['page_' . $entity_name]['display_options']['defaults']['filters'] = FALSE;
    }
  }
}
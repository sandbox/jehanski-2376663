<?php

/**
 * @file
 * Contains \Drupal\content_overviews\Form\ContentOverviewsSettingsForm.
 */

namespace Drupal\content_overviews\Form;

use Drupal\content_overviews\Events\ContentOverviewsEventHandler;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContentOverviewsSettingsForm extends ConfigFormBase {

  /**
   * List of content-types
   * @var \array
   */
  protected $content_types;

  /**
   * Handler takes care of dispatching our events.
   *
   * @var \Drupal\content_overviews\Events\ContentOverviewsEventHandler
   */
  protected $event_handler;

  /**
   * Constructs a new ContentOverviewsSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\content_overviews\Events\ContentOverviewsEventHandler $event_handler
   *   An interface for entity type managers.
   */
  function __construct(ConfigFactoryInterface $config_factory, ContentOverviewsEventHandler $event_handler) {
    parent::__construct($config_factory);

    $this->event_handler = $event_handler;
    $this->content_types = node_type_get_names();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('content_overviews.content_overviews_event_handler')
    );
  }

  /**
   * Returns a unique string identifying the form
   * @return string
   */
  public function getFormId() {
    return 'content_overviews_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $managed_types = \Drupal::config('content_overviews.settings')->get('managed_content_types', array());

    $form['co'] = array(
      '#type' => 'fieldset',
      '#title' => t('Managed modules'),
      '#description' => t('Choose the module(s) for which a content view should be generated.'),
      '#collapsible' => FALSE,
    );

    $form['co']['managed_content_types'] = array(
        '#type' => 'checkboxes',
        '#options' => $this->content_types,
        '#default_value' => empty($managed_types) ? array() : array_keys($managed_types),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->event_handler->handleContentTypeConfigAndEvent(
      'managed_content_types',
      $form_state->getValue('managed_content_types'),
      'content_types_changed'
    );

    parent::submitForm($form, $form_state);
  }
}
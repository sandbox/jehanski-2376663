<?php
namespace Drupal\content_overviews;


use Drupal\field\Entity\FieldConfig;

class ContentOverviewsFieldConfig {

  private $field;

  function __construct($entity_name, $field_name) {
    $this->field = FieldConfig::loadByName('node', $entity_name, $field_name);
  }

  /**
   * @return static
   */
  public function getField() {
    return $this->field;
  }

  public function getViewsSortInfo() {
    $field = array();
    switch ($this->field->field_type) {
      case 'text_long':
      case 'text_with_summary':
        $suffix = '_format';
        break;
      case 'taxonomy_term_reference':
        $suffix = '_target_id';
        break;
      default:
        $suffix = '_value';
    }

    $field['id'] = $this->field->field_name . $suffix;
    $field['table'] = 'node__' . $this->field->field_name;
    $field['field'] = $this->field->field_name . $suffix;
    $field['relationship'] = 'none';
    $field['group_type'] = 'group';
    $field['plugin_id'] = 'standard';
    $field['order'] = 'ASC';
    $field['exposed'] = TRUE;
    $field['expose'] = array(
      'order' => 'ASC',
      'label' => $this->field->label,
    );

    return $field;
  }

  public function getViewsExposedInfo() {
    $field = array();
    switch ($this->field->field_type) {
      case 'text':
      case 'string':
      case 'text_long':
      case 'string_long':
      case 'text_with_summary':
        $field['id'] = $this->field->field_name . '_value';
        $field['table'] = 'node__' . $this->field->field_name;
        $field['field'] = $this->field->field_name . '_value';
        $field['operator'] = 'contains';
        $field['exposed'] = TRUE;
        $field['expose']['operator_id'] = $this->field->field_name . '_value_op';
        $field['expose']['label'] = $this->field->label;
        $field['expose']['operator'] = $this->field->field_name . '_value_op';
        $field['expose']['identifier'] = $this->field->field_name . '_value';
        $field['expose']['remember_roles'] = array();

        break;

    }
    return $field;
  }

  public function generateContentOverviewsFieldConfig() {
    $co_field_config = array();

    $co_field_config['id'] = $this->field->field_name;
    $co_field_config['table'] = 'node__' . $this->field->field_name;
    $co_field_config['field'] = $this->field->field_name;
    $co_field_config['label'] = $this->field->label;
    $co_field_config['click_sort_column'] = 'value';
    $co_field_config['group_column'] = 'value';
    $co_field_config['relationship'] = 'none';
    $co_field_config['group_type'] = 'group';
    $co_field_config['plugin_id'] = 'field';
    $co_field_config['click_sort_column'] = 'value';
    $co_field_config['exclude'] = FALSE;
    $co_field_config['group_rows'] = TRUE;
    $co_field_config['delta_limit'] = 'all';
    $co_field_config['delta_offset'] = 0;
    $co_field_config['multi_type'] = 'separator';
    $co_field_config['separator'] = ', ';
    $co_field_config['field_api_classes'] = FALSE;


    // TODO: Add some more default values
    // Add extra styling based on the field type.
    switch ($this->field->field_type) {
      case 'image':
        $co_field_config['click_sort_column'] = 'fid';
        $co_field_config['settings'] = array(
          'image_style' => 'thumbnail',
          'image_link' => '',
        );
        break;

      case 'text_long':
      case 'string_long':
        // TODO: Check trim_length as it doesn't work right now.
        $co_field_config['type'] = 'text_trimmed';
        $co_field_config['settings'] = array(
          'trim_length' => 50,
        );
      case 'text_with_summary':
        $co_field_config['type'] = 'text_summary_or_trimmed';
        break;
      case 'date':
        $co_field_config['type'] = 'datetime_default';
        $co_field_config['settings'] = array(
          'format_type' => 'short'
        );
        break;
      case 'taxonomy_term_reference':
        $co_field_config['type'] = 'taxonomy_term_reference_link';
        $co_field_config['click_sort_column'] = 'target_id';
        $co_field_config['group_column'] = 'target_id';
        break;
    }
    return array($this->field->field_name => $co_field_config);
  }

} 
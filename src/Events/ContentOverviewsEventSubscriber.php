<?php

/**
 * @file
 * Contains Drupal\event_dispatcher_demo\EventSubscriber\ConfigSubscriber.
 */

namespace Drupal\content_overviews\Events;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\content_overviews\ContentOverviewsViewDisplayHandler;

class ContentOverviewsEventSubscriber implements EventSubscriberInterface {

  /**
   * Handler takes care of dispatching our events.
   *
   * @var \Drupal\content_overviews\Events\ContentOverviewsEventHandler
   */
  protected $event_handler;

  /**
   * Handler takes care of generating our views.
   *
   * @var \Drupal\content_overviews\ContentOverviewsViewDisplayHandler
   */
  protected $displayHandler;

  /**
   * Class constructor.
   *
   * @param \Drupal\content_overviews\Events\ContentOverviewsEventHandler $event_handler
   */
  public function __construct(ContentOverviewsEventHandler $event_handler) {
    $this->event_handler = $event_handler;
    $this->displayHandler = new ContentOverviewsViewDisplayHandler();
  }

  static function getSubscribedEvents() {
    $events['content_overviews.new_displays'][] = array('onNewDisplays', 0);
    $events['content_overviews.content_types_changed'][] = array(
      'onChangedContentType',
      0
    );
    $events['content_overviews.managed_fields_changed'][] = array(
      'onChangedManagedFields',
      0
    );
    $events['content_overviews.removed_displays'][] = array(
      'onRemovedDisplays',
      0
    );

    return $events;
  }

  public function onNewDisplays(ContentOverviewsEvent $event) {
    $menu_link_manager = \Drupal::service('plugin.manager.menu.link');

    foreach ($event->getEventData() as $entity => $route_name) {
      $types  = node_type_get_names();
      $title = $types[$entity];
      $menu_link_manager->addDefinition('co-overviews-' . $entity, array(
        'menu_name' => 'content-overviews',
        'title' => $title,
        'description' => 'Overview link for ' . $entity,
        'route_name' => $route_name,
        'class' => 'Drupal\content_overviews\Plugin\Menu\ContentOverviewsMenuLink',
      ));
    }
  }

  public function onRemovedDisplays(ContentOverviewsEvent $event) {
    $menu_link_manager = \Drupal::service('plugin.manager.menu.link');

    foreach ($event->getEventData() as $entity => $route_name) {
      $menu_link_manager->removeDefinition('co-overviews-' . $entity);
    }
  }

  public function onChangedContentType(ContentOverviewsEvent $event) {
    $enabled = array();
    $disabled = array();
    foreach ($event->getEventData() as $ct_machine_name => $status) {
      if ($status) {
        $enabled[] = $ct_machine_name;
      }
      else {
        $disabled[] = $ct_machine_name;
      }
    }

    $changes = $this->displayHandler->addDisplaysToView($enabled);
    if (count($changes)) {
      if (count($changes['added'])) {
        $this->event_handler->handleEvent('new_displays', $changes['added']);
      }
      if (count($changes['updated'])) {
        $this->event_handler->handleEvent('updated_displays', $changes['updated']);
      }
    }

    $removed_displays = $this->displayHandler->removeDisplaysFromView($disabled);
    if (count($removed_displays)) {
      $this->event_handler->handleEvent('removed_displays', $removed_displays);
    }
  }

  public function onChangedManagedFields(ContentOverviewsEvent $event) {
    $data = $event->getEventData();
    $bundle = $data['bundle'];
    $field_name = $data['field'];

    $this->displayHandler->addFieldToDisplay($bundle, $field_name);
  }

}
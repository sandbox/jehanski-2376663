<?php

namespace Drupal\content_overviews\Events;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class ContentOverviewsEventHandler {

  /**
   * An interface to dispatch our events
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $event_dispatcher;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new ContentOverviewsSettingsForm.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An interface for dispatching events.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  function __construct(EventDispatcherInterface $event_dispatcher, LoggerInterface $logger) {
    $this->event_dispatcher = $event_dispatcher;
    $this->logger = $logger;
  }

  /**
   * Handles storage of content-type configuration and dispatching of event
   *
   * @param $config_name
   * @param array $config_data
   * @param $event_name
   */
  public function handleContentTypeConfigAndEvent($config_name, array $config_data, $event_name) {
    if(empty($config_name)) {
      throw new InvalidArgumentException('CO - config name missing.');
    }

    if(!empty($config_data)) {
      $drupal_config = \Drupal::config('content_overviews.settings');
      $all_enabled_config = $drupal_config->get('managed_content_types', array());

      $changed_config = array();
      foreach($config_data as $node_type_name => $enabled) {
        $changed_config[$node_type_name] = empty($enabled) ? FALSE : TRUE;
        if($enabled) {
          $all_enabled_config[$node_type_name] = TRUE;
        } else {
          unset($all_enabled_config[$node_type_name]);
        }
      }

      $drupal_config->set($config_name, $all_enabled_config);
      $drupal_config->save();

      $this->handleEvent($event_name, $changed_config);

    } else {
      $this->logger->warning("No config saved for $config_name. Data is missing.");
    }
  }

  /**
   * Handles the event creation and dispatch
   *
   * @param $event_name (without prefix)
   * @param array $event_data (all changed)
   */
  public function handleEvent($event_name, array $event_data){
    if(empty($event_name)) {
      throw new InvalidArgumentException('CO - Event name missing.');
    }

    if(!empty($event_data)) {
      $e = new ContentOverviewsEvent($event_data);
      $this->event_dispatcher->dispatch("content_overviews.$event_name", $e);
    } else {
      $this->logger->warning("No event dispatched for $event_name. Data is missing.");
    }
  }

}
<?php

/**
 * @file
 * Contains Drupal\event_dispatcher_demo\DemoEvent.
 */

namespace Drupal\content_overviews\Events;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Config\Config;

class ContentOverviewsEvent extends Event {

  protected $eventData;

  /**
   * Constructor.
   *
   * @param array $eventData
   */
  public function __construct(array $eventData) {
    $this->eventData = $eventData;
  }

  /**
   * Getter for the eventData object.
   *
   * @return array
   */
  public function getEventData() {
    return $this->eventData;
  }

  /**
   * Setter for the config object.
   *
   * @param array $eventData
   */
  public function setEventData(array $eventData) {
    $this->eventData = $eventData;
  }

} 
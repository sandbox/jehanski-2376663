<?php

/**
 * @file
 * Contains \Drupal\content_overviews\Tests\ContentOverviewsTest.
 */

namespace Drupal\content_overviews\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests content overviews module.
 *
 * @group content_overviews
 */
class ContentOverviewsTest extends WebTestBase {

  protected $admin_user;

  protected function setUp() {
    parent::setUp();

    // Create and login administrative user.
    $all_permissions = array_keys(\Drupal::service('user.permissions')->getPermissions());
    $admin_user = $this->drupalCreateUser(
      $all_permissions,
      'test-admin-co'
    );

    $this->drupalLogin($admin_user);
  }
  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('node', 'views', 'field', 'menu_link_content', 'menu_ui', 'content_overviews');

  /**
   * Tests configuration
   */
  function testContentOverviewsSaveSettings() {
    $this->drupalGet('admin/config/content/co');
    $this->assertNoFieldChecked("edit-managed-content-types-node");

    $input = array('managed_content_types[node]' => 'node');
    $this->drupalPostForm('admin/config/content/co', $input, t('Save configuration'));
    $this->assertText(t('The configuration options have been saved.'));

    $this->assertFieldChecked("edit-managed-content-types-node");
  }
}
